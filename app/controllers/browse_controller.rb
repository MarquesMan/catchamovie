class BrowseController < ApplicationController
    before_action :check_profile
    def index
        @certif = Tmdb::Certification.movie_list
        @sugestions = Tmdb::Discover.movie(page: 2)

        @config = Tmdb::Configuration.get
        @base_url = @config.images.base_url + @config.images.poster_sizes[6]
    end

    def check_profile
        @not_checked = true

        if params.has_key?(:profile) && params[:profile] != ""     
            @profile_id = params.require(:profile).to_i
            @profiles = Profile.where(user_id: current_user.id ).take(4)
            @number_of_profiles = @profiles.size()

            if (@profile_id > 0) && (@profile_id <= @number_of_profiles)
                @profile = @profiles[@profile_id-1]
                @colorStrings = ['bg-warning', 'bg-primary', 'bg-success', 'bg-danger']
                @not_checked = false
            end

        end
        
        if @not_checked
            redirect_to root_path
        end
    end

end
