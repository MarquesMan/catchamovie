class ImdbInterfaceController < ApplicationController
    before_action :check_profile

    def discover
        respond_to do |format|
            begin
                format.html # show.html.erb
                if params.has_key?(:keywords) && params[:keywords] != ""    
                    list_of_movies = Array.new([])
                    movies =  Tmdb::Search.movie(params.require(:keywords)).results 
                    format.json { render json: movies }
                else
                    
                    genres = GenresByProfile.where(profile_id: @profile.id ).take(2)
                    list_of_genres = ""

                    genres.each do |genre|
                        list_of_genres+= genre.genre_id.to_s+","
                    end
                    
                    if list_of_genres != ""  
                        format.json { render json: Tmdb::Discover.movie(page: params.require(:page), with_genres: list_of_genres.at(0..-1)).results }
                    else
                        format.json { render json: Tmdb::Discover.movie(page: params.require(:page)).results }  
                    end      
                end
            rescue StandardError => e
                format.html # show.html.erb
                format.json { render json: e }
            end
        end
    end

    def search
    end

    def watched_list
        respond_to do |format|
            begin
                list_of_movies = Array.new([])
                
                MoviesByProfile.find_each do |movie|
                    if movie.seen && (movie.profile_id == @profile.id)
                        list_of_movies.push( Tmdb::Movie.detail(movie.movie_id) )
                    end
                end

                format.html # show.html.erb
                format.json { render json: list_of_movies }
            rescue StandardError => e
                format.html # show.html.erb
                format.json { render json: e }
            end
        end
    end

    def to_watch_list
        respond_to do |format|
            begin
                list_of_movies = Array.new([])
                
                MoviesByProfile.find_each do |movie|
                    if (movie.seen == false) && (movie.profile_id == @profile.id)
                        list_of_movies.push( Tmdb::Movie.detail(movie.movie_id) )
                    end
                end

                format.html # show.html.erb
                format.json { render json: list_of_movies }
            rescue StandardError => e
                format.html # show.html.erb
                format.json { render json: e }
            end
        end
    end

    def watched
        respond_to do |format|
            begin
                @id = params.require(:movie_id)
                
                movie = MoviesByProfile.find_by(profile_id: @profile.id, movie_id: @id)
                movie.update(seen: 1)

                format.html # show.html.erb
                format.json { render json: true }
            rescue StandardError => e
                format.html # show.html.erb
                format.json { render json: e }
            end
        end
    end

    def interested
        respond_to do |format|
            begin
                @id = params.require(:movie_id)
                
                Tmdb::Movie.detail(@id).genres.each do |genre|
                    GenresByProfile.create(profile_id: @profile.id, genre_id:genre.id)
                end
                
                MoviesByProfile.create(profile_id: @profile.id, movie_id: @id)
                format.html # show.html.erb
                format.json { render json: true }
            rescue StandardError => e
                format.html # show.html.erb
                format.json { render json: e }
            end
        end
    end

    def check_profile
        @not_checked = true

        if params.has_key?(:profile) && params[:profile] != ""     
            @profile_id = params.require(:profile).to_i
            @profiles = Profile.where(user_id: current_user.id ).take(4)
            @number_of_profiles = @profiles.size()

            if (@profile_id > 0) && (@profile_id <= @number_of_profiles)
                @profile = @profiles[@profile_id-1]
                @not_checked = false
            end

        end
        
        if @not_checked
            respond_to do |format|
                    format.html # show.html.erb
                    format.json { render json: [] }
            end
        end
    end
    
end
