class HomeController < ApplicationController
    def index
        @profiles = Profile.where(user_id: current_user.id ).take(4)
        @number_of_profiles = @profiles.size()
        @color_strings = ['bg-warning', 'bg-primary', 'bg-success', 'bg-danger']
    end

    def addProfile

        @profiles = Profile.where(user_id: current_user.id ).take(4)
        @number_of_profiles = @profiles.size()

        if params.has_key?(:profileName) && params[:profileName] != ""     
            if (@number_of_profiles < 4)
                Profile.create(user_id: current_user.id, profile_name: params.require(:profileName))
            end
        end

        redirect_to  root_path
    end

end
