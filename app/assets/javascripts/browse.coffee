# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
    lastShowDeck = $("#sugestions-deck")
    lastShowTab =  $("#sugestions")

    lastShowTab.removeClass("btn-secondary")
    lastShowTab.addClass("btn-warning")
    $(".card-deck").hide()

   
    itm = document.getElementById("load");
    loadCard = itm.cloneNode(true);
    

    discover_page = 1
    
    $("label").on "click", (e) ->
        string = $(this)[0].id

        $("#watched-deck").empty()
        $("#interested-deck").empty()
        exchangeColor($(this), lastShowTab)

        lastShowDeck.hide()
        lastShowDeck = $("#"+string+"-deck")
        lastShowTab = $(this)

        if  string == "watched"
            $(".card-deck").hide()
            $(".loading").show()
            loadWatched()
        else if string == "interested"
            $(".card-deck").hide()
            $(".loading").show()
            loadInterested()
        else
            lastShowDeck.show()

    $("#load").on "click", (e) ->
        loadMoreSugestions(true)

    createMovieCard = (id,name, url) -> 
        div = document.createElement('div');
        div.setAttribute("class","card text-white bg-secondary mb-4")
        div.setAttribute("id", id)
        div.setAttribute("name", name)
        div.setAttribute("style", "width: 15rem;")

        img = document.createElement('img')
        img.setAttribute("class","card-img img-fluid")
        img.setAttribute("src", url)
        img.setAttribute("alt", "Card image cap")
        div.appendChild(img);
        return div



    exchangeColor = (newTab, oldTab) ->
        oldTab.removeClass("btn-warning")
        oldTab.addClass("btn-secondary")
        newTab.removeClass("btn-secondary")
        newTab.addClass("btn-warning")

    watch = () ->
        if confirm("Marcar para ver:\n"+this.attributes['name'].value)
            change_movie_status(this.attributes['id'].value, '/interested')

    watched = () ->
        if confirm("Marcar como visto:\n"+this.attributes['name'].value)
            change_movie_status(this.attributes['id'].value, '/watched')

    change_movie_status = (id, action) ->
         $.ajax action , 
            type: "GET",
            dataType: "JSON",
            data: 
                movie_id: id,
                profile: profile_id
            asnyc: false,
            success: (data) ->
                console.log(data)
                $( "#"+id ).remove();
            error: (data) ->
                console.log(data)


    loadMoreSugestions = (append = false, search_keywords = "" ) ->
        $.ajax '/discover' , 
            type: "GET",
            dataType: "JSON",
            data: 
                page: discover_page,
                profile: profile_id,
                keywords : search_keywords

            asnyc: false,
            success: (data) ->
                console.log(data)
                $(".loading").hide()
                card = document.getElementById("load");
                deck = document.getElementById("sugestions-deck");
                fragment = document.createDocumentFragment();
                for movie in data
                    movieCard = createMovieCard(movie.table.id,movie.table.title, base_url+movie.table.poster_path )
                    fragment.appendChild( movieCard )
                    $(movieCard).bind( "click",watch)

                if append         
                    deck.insertBefore(fragment, deck.lastChild.previousSibling)
                else
                    deck.appendChild(fragment)
                    deck.appendChild(loadCard) 
                    $(loadCard).on "click", (e) ->
                        loadMoreSugestions(true)

                discover_page+=1
                
                lastShowDeck.show()
                

            error: (data) ->
                console.log(data)

    loadInterested = () ->
        $.ajax '/to_watch_list' , 
            type: "GET",
            dataType: "JSON",
            asnyc: false,
            data: 
                profile: profile_id

            success: (data) ->
                
                deck = document.getElementById("interested-deck");
                fragment = document.createDocumentFragment();
                for movie in data
                    movieCard = createMovieCard(movie.table.id,movie.table.title, base_url+movie.table.poster_path )
                    fragment.appendChild( movieCard )
                    $(movieCard).bind( "click",watched)

                deck.appendChild(fragment)
                lastShowDeck.show()
                $(".loading").hide()

            error: (data) ->
                #alert(data.responseText)
                console.log(data) 

    loadWatched = () ->
        $.ajax '/watched_list' , 
            type: "GET",
            dataType: "JSON",
            asnyc: false,
            data: 
                profile: profile_id

            success: (data) ->
                console.log(data) 
                deck = document.getElementById("watched-deck");
                fragment = document.createDocumentFragment();
                for movie in data
                    fragment.appendChild( createMovieCard(movie.table.id,movie.table.title, base_url+movie.table.poster_path ) )
                deck.appendChild(fragment)
                lastShowDeck.show()
                $(".loading").hide()

            error: (data) ->
                #alert(data.responseText)
                console.log(data) 

    $("#search").on "keyup", (e) ->
        $("#sugestions-deck").empty()
        $(".loading").show()
        if(this.value == '')
            discover_page = 1
            loadMoreSugestions()
        else
            loadMoreSugestions(false,this.value)

    loadMoreSugestions(true)