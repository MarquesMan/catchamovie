Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  get 'browse/index'
  get '/addProfile/', to: 'home#addProfile'

  get '/discover', to: 'imdb_interface#discover'
  get '/search', to: 'imdb_interface#search'
  get '/watched', to: 'imdb_interface#watched'
  get '/interested', to: 'imdb_interface#interested'
  get '/watched_list', to: 'imdb_interface#watched_list'
  get '/to_watch_list', to: 'imdb_interface#to_watch_list'

end
