every '0 0 * * *' do
  rake "update_a_record", environment: :production
end

every '@reboot' do
  command "source /home/bitnami/.rvm/scripts/rvm && cd /home/bitnami/catchamovie && bundle exec rails s -b 0.0.0.0 -p 3000 -e production -d &"
end
