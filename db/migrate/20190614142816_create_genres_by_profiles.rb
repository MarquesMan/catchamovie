class CreateGenresByProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :genres_by_profiles do |t|
      t.belongs_to :profile, index: true, foreign_key: true
      t.belongs_to :genre, index: true, foreign_key: true
      t.integer :genres_count, :default => 0
       
      t.timestamps
    end
  end
end
