class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.belongs_to :user, index: true , foreign_key: true
      t.string :profile_name

      t.timestamps
    end
  end
end
