class CreateMoviesByProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :movies_by_profiles do |t|
      t.belongs_to :profile, index: true, foreign_key: true
      t.string :movie_id
      t.boolean :seen, :default => false

      t.timestamps
    end
  end
end
