# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Genre.create(id: 28, name:"Ação")
Genre.create(id: 12, name:"Aventura")
Genre.create(id: 16, name:"Animação")
Genre.create(id: 35, name:"Comédia")
Genre.create(id: 80, name:"Crime")
Genre.create(id: 99, name:"Documentário")
Genre.create(id: 18, name:"Drama")
Genre.create(id: 10751, name:"Família")
Genre.create(id: 14, name:"Fantasia")
Genre.create(id: 36, name:"História")
Genre.create(id: 27, name:"Terror")
Genre.create(id: 10402, name:"Música")
Genre.create(id: 9648, name:"Mistério")
Genre.create(id: 10749, name:"Romance")
Genre.create(id: 878, name:"Ficção científica")
Genre.create(id: 10770, name:"Cinema TV")
Genre.create(id: 53, name:"Thriller")
Genre.create(id: 10752, name:"Guerra")
Genre.create(id: 37, name:"Faroeste")